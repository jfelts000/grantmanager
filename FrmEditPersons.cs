﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrantManager
{
    public partial class FrmEditPersons : Form
    {
        public FrmEditPersons()
        {
            InitializeComponent();
        }

        private void personsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.personsBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.grantManagerDataSet);

        }

        private void FrmEditPersons_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'grantManagerDataSet.Persons' table. You can move, or remove it, as needed.
            this.personsTableAdapter.Fill(this.grantManagerDataSet.Persons);

        }

        private void personsDataGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
                    {
            string headerText = personsDataGridView.Columns[e.ColumnIndex].HeaderText;
            string cellvalue = e.FormattedValue.ToString();
            // Confirm that the cell is not empty.


            if (string.IsNullOrEmpty(e.FormattedValue.ToString()))
            {
                personsDataGridView.Rows[e.RowIndex].ErrorText =
                headerText + " cannot be empty";
                e.Cancel = true;
            }
            if (headerText == "PersonId")// | headerText == "LocationName")
            {
                foreach (DataGridViewRow row in personsDataGridView.Rows)
                {
                    foreach (DataGridViewColumn col in personsDataGridView.Columns)
                    {

                        if (row.Index < personsDataGridView.Rows.Count - 1)
                        {
                            if (col.Index != personsDataGridView.CurrentCell.ColumnIndex | row.Index != personsDataGridView.CurrentCell.RowIndex)
                            {
                                //Regex.IsMatch(LN1, LN2, RegexOptions.IgnoreCase
                                var compare = row.Cells[col.Index].Value.ToString();
                                if (col.HeaderText.ToString() == "PersonId") //| col.HeaderText.ToString() == "PersonName")
                                {
                                    if (System.Text.RegularExpressions.Regex.IsMatch(compare, "^" + cellvalue + "$", RegexOptions.IgnoreCase))
                                    {
                                        personsDataGridView.Rows[e.RowIndex].ErrorText = headerText + " cannot contain duplicate";
                                        e.Cancel = true;
                                    }
                                }



                            }

                        }
                    }
                }
            }
        }
        }
    }
}
