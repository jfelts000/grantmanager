﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace GrantManager
{
   
    public partial class FrmStart : Form
    {
        public FrmStart()
        {
            InitializeComponent();
        }

        private void editGrantsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form EditGrants = new FrmEditGrants();
            EditGrants.Show();
        }

        private void editPositionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
           Form EditPosiotions = new FrmEditPositions();
            EditPosiotions.Show();
        }

        private void editBenifitsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form EditBenifits = new FrmEditBenifits();
            EditBenifits.Show();
        }

        private void editLocationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form EditLocations = new FrmEditLocations();
            EditLocations.Show();
        }

        private void editPersonsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form EditPersons = new FrmEditPersons();
            EditPersons.Show();
        }

        private void editTimeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form EditTime = new FrmEditTime();
            EditTime.Show();
        }
    }

}
