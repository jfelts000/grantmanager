﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrantManager
{
    public partial class FrmEditTime : Form
    {
        public FrmEditTime()
        {
            InitializeComponent();
        }

        private void timeBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.timeBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.grantManagerDataSet);

        }

        private void FrmEditTime_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'grantManagerDataSet.Time' table. You can move, or remove it, as needed.
            this.timeTableAdapter.Fill(this.grantManagerDataSet.Time);

        }

        private void timeDataGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            string headerText = timeDataGridView.Columns[e.ColumnIndex].HeaderText;
            string cellvalue = e.FormattedValue.ToString();
            // Confirm that the cell is not empty.


            if (string.IsNullOrEmpty(e.FormattedValue.ToString()))
            {
                timeDataGridView.Rows[e.RowIndex].ErrorText =
                headerText + " cannot be empty";
                e.Cancel = true;
            }
            if (headerText == "PersonnId")// | headerText == "LocationName")
            {
                foreach (DataGridViewRow row in timeDataGridView.Rows)
                {
                    foreach (DataGridViewColumn col in timeDataGridView.Columns)
                    {

                        if (row.Index < timeDataGridView.Rows.Count - 1)
                        {
                            if (col.Index != timeDataGridView.CurrentCell.ColumnIndex | row.Index != timeDataGridView.CurrentCell.RowIndex)
                            {
                                //Regex.IsMatch(LN1, LN2, RegexOptions.IgnoreCase
                                var compare = row.Cells[col.Index].Value.ToString();
                                if (col.HeaderText.ToString() == "PersonnId")// | col.HeaderText.ToString() == "LocationName")
                                {
                                    if (System.Text.RegularExpressions.Regex.IsMatch(compare, "^" + cellvalue + "$", RegexOptions.IgnoreCase))
                                    {
                                        timeDataGridView.Rows[e.RowIndex].ErrorText = headerText + " cannot contain duplicate";
                                        e.Cancel = true;
                                    }
                                }



                            }

                        }
                    }
                }
            }
        }

        private void timeDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
    }
}
