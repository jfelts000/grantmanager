﻿namespace GrantManager
{
    partial class FrmEditBenifits
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEditBenifits));
            this.grantManagerDataSet = new GrantManager.GrantManagerDataSet();
            this.benifitsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.benifitsTableAdapter = new GrantManager.GrantManagerDataSetTableAdapters.BenifitsTableAdapter();
            this.tableAdapterManager = new GrantManager.GrantManagerDataSetTableAdapters.TableAdapterManager();
            this.benifitsBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.benifitsBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.benifitsDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.grantManagerDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.benifitsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.benifitsBindingNavigator)).BeginInit();
            this.benifitsBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.benifitsDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // grantManagerDataSet
            // 
            this.grantManagerDataSet.DataSetName = "GrantManagerDataSet";
            this.grantManagerDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // benifitsBindingSource
            // 
            this.benifitsBindingSource.DataMember = "Benifits";
            this.benifitsBindingSource.DataSource = this.grantManagerDataSet;
            // 
            // benifitsTableAdapter
            // 
            this.benifitsTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BenifitsTableAdapter = this.benifitsTableAdapter;
            this.tableAdapterManager.GrantsTableAdapter = null;
            this.tableAdapterManager.LocationsTableAdapter = null;
            this.tableAdapterManager.PersonsTableAdapter = null;
            this.tableAdapterManager.PositionsTableAdapter = null;
            this.tableAdapterManager.TimeTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = GrantManager.GrantManagerDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // benifitsBindingNavigator
            // 
            this.benifitsBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.benifitsBindingNavigator.BindingSource = this.benifitsBindingSource;
            this.benifitsBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.benifitsBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.benifitsBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.benifitsBindingNavigatorSaveItem});
            this.benifitsBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.benifitsBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.benifitsBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.benifitsBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.benifitsBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.benifitsBindingNavigator.Name = "benifitsBindingNavigator";
            this.benifitsBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.benifitsBindingNavigator.Size = new System.Drawing.Size(497, 25);
            this.benifitsBindingNavigator.TabIndex = 0;
            this.benifitsBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // benifitsBindingNavigatorSaveItem
            // 
            this.benifitsBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.benifitsBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("benifitsBindingNavigatorSaveItem.Image")));
            this.benifitsBindingNavigatorSaveItem.Name = "benifitsBindingNavigatorSaveItem";
            this.benifitsBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.benifitsBindingNavigatorSaveItem.Text = "Save Data";
            this.benifitsBindingNavigatorSaveItem.Click += new System.EventHandler(this.benifitsBindingNavigatorSaveItem_Click);
            // 
            // benifitsDataGridView
            // 
            this.benifitsDataGridView.AutoGenerateColumns = false;
            this.benifitsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.benifitsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.benifitsDataGridView.DataSource = this.benifitsBindingSource;
            this.benifitsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.benifitsDataGridView.Location = new System.Drawing.Point(0, 25);
            this.benifitsDataGridView.Name = "benifitsDataGridView";
            this.benifitsDataGridView.Size = new System.Drawing.Size(497, 321);
            this.benifitsDataGridView.TabIndex = 1;
            this.benifitsDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.benifitsDataGridView_CellContentClick);
            this.benifitsDataGridView.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.benifitsDataGridView_CellValidating);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "BenifitId";
            this.dataGridViewTextBoxColumn1.HeaderText = "BenifitId";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "BenifitName";
            this.dataGridViewTextBoxColumn2.HeaderText = "BenifitName";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // FrmEditBenifits
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(497, 346);
            this.Controls.Add(this.benifitsDataGridView);
            this.Controls.Add(this.benifitsBindingNavigator);
            this.Name = "FrmEditBenifits";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.FrmEditBenifits_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grantManagerDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.benifitsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.benifitsBindingNavigator)).EndInit();
            this.benifitsBindingNavigator.ResumeLayout(false);
            this.benifitsBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.benifitsDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private GrantManagerDataSet grantManagerDataSet;
        private System.Windows.Forms.BindingSource benifitsBindingSource;
        private GrantManagerDataSetTableAdapters.BenifitsTableAdapter benifitsTableAdapter;
        private GrantManagerDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator benifitsBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton benifitsBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView benifitsDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    }
}