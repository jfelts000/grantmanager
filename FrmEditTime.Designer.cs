﻿namespace GrantManager
{
    partial class FrmEditTime
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEditTime));
            this.grantManagerDataSet = new GrantManager.GrantManagerDataSet();
            this.timeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.timeTableAdapter = new GrantManager.GrantManagerDataSetTableAdapters.TimeTableAdapter();
            this.tableAdapterManager = new GrantManager.GrantManagerDataSetTableAdapters.TableAdapterManager();
            this.timeBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.timeBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.timeDataGridView = new System.Windows.Forms.DataGridView();
            this.personIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.personNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timeTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.payDesignationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalHrsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dollarsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deptDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.payPeriodDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.grantManagerDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeBindingNavigator)).BeginInit();
            this.timeBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.timeDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // grantManagerDataSet
            // 
            this.grantManagerDataSet.DataSetName = "GrantManagerDataSet";
            this.grantManagerDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // timeBindingSource
            // 
            this.timeBindingSource.DataMember = "Time";
            this.timeBindingSource.DataSource = this.grantManagerDataSet;
            // 
            // timeTableAdapter
            // 
            this.timeTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BenifitsTableAdapter = null;
            this.tableAdapterManager.GrantsTableAdapter = null;
            this.tableAdapterManager.LocationsTableAdapter = null;
            this.tableAdapterManager.PersonsTableAdapter = null;
            this.tableAdapterManager.PositionsTableAdapter = null;
            this.tableAdapterManager.TimeTableAdapter = this.timeTableAdapter;
            this.tableAdapterManager.UpdateOrder = GrantManager.GrantManagerDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // timeBindingNavigator
            // 
            this.timeBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.timeBindingNavigator.BindingSource = this.timeBindingSource;
            this.timeBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.timeBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.timeBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.timeBindingNavigatorSaveItem});
            this.timeBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.timeBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.timeBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.timeBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.timeBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.timeBindingNavigator.Name = "timeBindingNavigator";
            this.timeBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.timeBindingNavigator.Size = new System.Drawing.Size(945, 25);
            this.timeBindingNavigator.TabIndex = 0;
            this.timeBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // timeBindingNavigatorSaveItem
            // 
            this.timeBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.timeBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("timeBindingNavigatorSaveItem.Image")));
            this.timeBindingNavigatorSaveItem.Name = "timeBindingNavigatorSaveItem";
            this.timeBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.timeBindingNavigatorSaveItem.Text = "Save Data";
            this.timeBindingNavigatorSaveItem.Click += new System.EventHandler(this.timeBindingNavigatorSaveItem_Click);
            // 
            // timeDataGridView
            // 
            this.timeDataGridView.AutoGenerateColumns = false;
            this.timeDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.timeDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.personIdDataGridViewTextBoxColumn,
            this.personNameDataGridViewTextBoxColumn,
            this.dateDataGridViewTextBoxColumn,
            this.timeTypeDataGridViewTextBoxColumn,
            this.payDesignationDataGridViewTextBoxColumn,
            this.totalHrsDataGridViewTextBoxColumn,
            this.dollarsDataGridViewTextBoxColumn,
            this.deptDataGridViewTextBoxColumn,
            this.payPeriodDataGridViewTextBoxColumn});
            this.timeDataGridView.DataSource = this.timeBindingSource;
            this.timeDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.timeDataGridView.Location = new System.Drawing.Point(0, 25);
            this.timeDataGridView.Name = "timeDataGridView";
            this.timeDataGridView.Size = new System.Drawing.Size(945, 329);
            this.timeDataGridView.TabIndex = 1;
            this.timeDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.timeDataGridView_CellContentClick);
            this.timeDataGridView.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.timeDataGridView_CellValidating);
            // 
            // personIdDataGridViewTextBoxColumn
            // 
            this.personIdDataGridViewTextBoxColumn.DataPropertyName = "PersonId";
            this.personIdDataGridViewTextBoxColumn.HeaderText = "PersonId";
            this.personIdDataGridViewTextBoxColumn.Name = "personIdDataGridViewTextBoxColumn";
            // 
            // personNameDataGridViewTextBoxColumn
            // 
            this.personNameDataGridViewTextBoxColumn.DataPropertyName = "PersonName";
            this.personNameDataGridViewTextBoxColumn.HeaderText = "PersonName";
            this.personNameDataGridViewTextBoxColumn.Name = "personNameDataGridViewTextBoxColumn";
            // 
            // dateDataGridViewTextBoxColumn
            // 
            this.dateDataGridViewTextBoxColumn.DataPropertyName = "Date";
            this.dateDataGridViewTextBoxColumn.HeaderText = "Date";
            this.dateDataGridViewTextBoxColumn.Name = "dateDataGridViewTextBoxColumn";
            // 
            // timeTypeDataGridViewTextBoxColumn
            // 
            this.timeTypeDataGridViewTextBoxColumn.DataPropertyName = "TimeType";
            this.timeTypeDataGridViewTextBoxColumn.HeaderText = "TimeType";
            this.timeTypeDataGridViewTextBoxColumn.Name = "timeTypeDataGridViewTextBoxColumn";
            // 
            // payDesignationDataGridViewTextBoxColumn
            // 
            this.payDesignationDataGridViewTextBoxColumn.DataPropertyName = "PayDesignation";
            this.payDesignationDataGridViewTextBoxColumn.HeaderText = "PayDesignation";
            this.payDesignationDataGridViewTextBoxColumn.Name = "payDesignationDataGridViewTextBoxColumn";
            // 
            // totalHrsDataGridViewTextBoxColumn
            // 
            this.totalHrsDataGridViewTextBoxColumn.DataPropertyName = "TotalHrs";
            this.totalHrsDataGridViewTextBoxColumn.HeaderText = "TotalHrs";
            this.totalHrsDataGridViewTextBoxColumn.Name = "totalHrsDataGridViewTextBoxColumn";
            // 
            // dollarsDataGridViewTextBoxColumn
            // 
            this.dollarsDataGridViewTextBoxColumn.DataPropertyName = "Dollars";
            this.dollarsDataGridViewTextBoxColumn.HeaderText = "Dollars";
            this.dollarsDataGridViewTextBoxColumn.Name = "dollarsDataGridViewTextBoxColumn";
            // 
            // deptDataGridViewTextBoxColumn
            // 
            this.deptDataGridViewTextBoxColumn.DataPropertyName = "Dept";
            this.deptDataGridViewTextBoxColumn.HeaderText = "Dept";
            this.deptDataGridViewTextBoxColumn.Name = "deptDataGridViewTextBoxColumn";
            // 
            // payPeriodDataGridViewTextBoxColumn
            // 
            this.payPeriodDataGridViewTextBoxColumn.DataPropertyName = "PayPeriod";
            this.payPeriodDataGridViewTextBoxColumn.HeaderText = "PayPeriod";
            this.payPeriodDataGridViewTextBoxColumn.Name = "payPeriodDataGridViewTextBoxColumn";
            // 
            // FrmEditTime
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(945, 354);
            this.Controls.Add(this.timeDataGridView);
            this.Controls.Add(this.timeBindingNavigator);
            this.Name = "FrmEditTime";
            this.Text = "FrmEditTime";
            this.Load += new System.EventHandler(this.FrmEditTime_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grantManagerDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeBindingNavigator)).EndInit();
            this.timeBindingNavigator.ResumeLayout(false);
            this.timeBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.timeDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private GrantManagerDataSet grantManagerDataSet;
        private System.Windows.Forms.BindingSource timeBindingSource;
        private GrantManagerDataSetTableAdapters.TimeTableAdapter timeTableAdapter;
        private GrantManagerDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator timeBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton timeBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView timeDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn personIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn personNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn timeTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn payDesignationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalHrsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dollarsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn deptDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn payPeriodDataGridViewTextBoxColumn;
    }
}