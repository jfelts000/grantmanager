﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrantManager
{
    public partial class FrmEditGrants : Form
    {
        public FrmEditGrants()
        {
            InitializeComponent();
        }

  private void grantsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
  {
    this.Validate();
    this.grantsBindingSource.EndEdit();
    this.tableAdapterManager.UpdateAll(this.grantManagerDataSet);
            
  } 




   private void FrmEditGrants_Load(object sender, EventArgs e)
   {
        // TODO: This line of code loads data into the 'grantManagerDataSet.Grants' table. You can move, or remove it, as needed.
        this.grantsTableAdapter.Fill(this.grantManagerDataSet.Grants);

   }

   private void grantsDataGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
   {           
        string headerText = grantsDataGridView.Columns[e.ColumnIndex].HeaderText;
        string cellvalue = e.FormattedValue.ToString();
            // Confirm that the cell is not empty.


                if (string.IsNullOrEmpty(e.FormattedValue.ToString()))
                {
                    grantsDataGridView.Rows[e.RowIndex].ErrorText =
                    headerText + " cannot be empty";
                    e.Cancel = true;
                }
            if (headerText == "GrantId" | headerText == "GrantName")
            {
                foreach (DataGridViewRow row in grantsDataGridView.Rows)
                { 
                    foreach (DataGridViewColumn col in grantsDataGridView.Columns)
                    {

                        if (row.Index < grantsDataGridView.Rows.Count - 1)
                        {
                            if (col.Index != grantsDataGridView.CurrentCell.ColumnIndex | row.Index != grantsDataGridView.CurrentCell.RowIndex)
                            {                                
                                    //Regex.IsMatch(LN1, LN2, RegexOptions.IgnoreCase
                                    var compare = row.Cells[col.Index].Value.ToString();
                                    if(col.HeaderText.ToString() == "GrantName" | col.HeaderText.ToString() == "GrantId")
                                    {
                                        if (Regex.IsMatch(compare,  "^"+cellvalue+"$", RegexOptions.IgnoreCase))
                                        {
                                            grantsDataGridView.Rows[e.RowIndex].ErrorText = headerText + " cannot contain duplicate";
                                            e.Cancel = true;
                                        }
                                }


                                       
                                    }

                                }
                            }
                        }
                    }
                }


            }


            }
        


    

