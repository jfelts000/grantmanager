﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrantManager
{
    public partial class FrmEditLocations : Form
    {
        public FrmEditLocations()
        {
            InitializeComponent();
        }

        private void locationsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.locationsBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.grantManagerDataSet);

        }

        private void FrmEditLocations_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'grantManagerDataSet.Locations' table. You can move, or remove it, as needed.
            this.locationsTableAdapter.Fill(this.grantManagerDataSet.Locations);

        }

        private void locationsDataGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            string headerText = locationsDataGridView.Columns[e.ColumnIndex].HeaderText;
            string cellvalue = e.FormattedValue.ToString();
            // Confirm that the cell is not empty.


            if (string.IsNullOrEmpty(e.FormattedValue.ToString()))
            {
                locationsDataGridView.Rows[e.RowIndex].ErrorText =
                headerText + " cannot be empty";
                e.Cancel = true;
            }
            if (headerText == "LocationId" | headerText == "LocationName")
            {
                foreach (DataGridViewRow row in locationsDataGridView.Rows)
                {
                    foreach (DataGridViewColumn col in locationsDataGridView.Columns)
                    {

                        if (row.Index < locationsDataGridView.Rows.Count - 1)
                        {
                            if (col.Index != locationsDataGridView.CurrentCell.ColumnIndex | row.Index != locationsDataGridView.CurrentCell.RowIndex)
                            {
                                //Regex.IsMatch(LN1, LN2, RegexOptions.IgnoreCase
                                var compare = row.Cells[col.Index].Value.ToString();
                                if (col.HeaderText.ToString() == "LocationId" | col.HeaderText.ToString() == "LocationName")
                                {
                                    if (Regex.IsMatch(compare, "^" + cellvalue + "$", RegexOptions.IgnoreCase))
                                    {
                                        locationsDataGridView.Rows[e.RowIndex].ErrorText = headerText + " cannot contain duplicate";
                                        e.Cancel = true;
                                    }
                                }



                            }

                        }
                    }
                }
            }
        }
    }
}
