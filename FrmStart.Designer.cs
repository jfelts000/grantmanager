﻿namespace GrantManager
{
    partial class FrmStart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.databaseOperationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editGrantsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editPositionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editPersonsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editLocationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editBenifitsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editTimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.databaseOperationsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(728, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // databaseOperationsToolStripMenuItem
            // 
            this.databaseOperationsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editGrantsToolStripMenuItem,
            this.editPositionsToolStripMenuItem,
            this.editPersonsToolStripMenuItem,
            this.editLocationsToolStripMenuItem,
            this.editBenifitsToolStripMenuItem,
            this.editTimeToolStripMenuItem});
            this.databaseOperationsToolStripMenuItem.Name = "databaseOperationsToolStripMenuItem";
            this.databaseOperationsToolStripMenuItem.Size = new System.Drawing.Size(128, 20);
            this.databaseOperationsToolStripMenuItem.Text = "Database Operations";
            // 
            // editGrantsToolStripMenuItem
            // 
            this.editGrantsToolStripMenuItem.Name = "editGrantsToolStripMenuItem";
            this.editGrantsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.editGrantsToolStripMenuItem.Text = "Edit Grants";
            this.editGrantsToolStripMenuItem.Click += new System.EventHandler(this.editGrantsToolStripMenuItem_Click);
            // 
            // editPositionsToolStripMenuItem
            // 
            this.editPositionsToolStripMenuItem.Name = "editPositionsToolStripMenuItem";
            this.editPositionsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.editPositionsToolStripMenuItem.Text = "Edit Positions";
            this.editPositionsToolStripMenuItem.Click += new System.EventHandler(this.editPositionsToolStripMenuItem_Click);
            // 
            // editPersonsToolStripMenuItem
            // 
            this.editPersonsToolStripMenuItem.Name = "editPersonsToolStripMenuItem";
            this.editPersonsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.editPersonsToolStripMenuItem.Text = "Edit Persons";
            this.editPersonsToolStripMenuItem.Click += new System.EventHandler(this.editPersonsToolStripMenuItem_Click);
            // 
            // editLocationsToolStripMenuItem
            // 
            this.editLocationsToolStripMenuItem.Name = "editLocationsToolStripMenuItem";
            this.editLocationsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.editLocationsToolStripMenuItem.Text = "Edit Locations";
            this.editLocationsToolStripMenuItem.Click += new System.EventHandler(this.editLocationsToolStripMenuItem_Click);
            // 
            // editBenifitsToolStripMenuItem
            // 
            this.editBenifitsToolStripMenuItem.Name = "editBenifitsToolStripMenuItem";
            this.editBenifitsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.editBenifitsToolStripMenuItem.Text = "Edit Benifits";
            this.editBenifitsToolStripMenuItem.Click += new System.EventHandler(this.editBenifitsToolStripMenuItem_Click);
            // 
            // editTimeToolStripMenuItem
            // 
            this.editTimeToolStripMenuItem.Name = "editTimeToolStripMenuItem";
            this.editTimeToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.editTimeToolStripMenuItem.Text = "Edit Time";
            this.editTimeToolStripMenuItem.Click += new System.EventHandler(this.editTimeToolStripMenuItem_Click);
            // 
            // FrmStart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(728, 312);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmStart";
            this.Text = "Grant Manager V 0.1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem databaseOperationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editGrantsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editPositionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editPersonsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editLocationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editBenifitsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editTimeToolStripMenuItem;
    }
}

