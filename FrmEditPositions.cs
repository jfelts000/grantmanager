﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrantManager
{
    public partial class FrmEditPositions : Form
    {
        public FrmEditPositions()
        {
            InitializeComponent();
        }

        private void positionsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.positionsBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.grantManagerDataSet);

        }

        private void FrmEditPositions_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'grantManagerDataSet.Positions' table. You can move, or remove it, as needed.
            this.positionsTableAdapter.Fill(this.grantManagerDataSet.Positions);

        }

        private void positionsDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            

        }

        private void positionsDataGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            string headerText = positionsDataGridView.Columns[e.ColumnIndex].HeaderText;
            string cellvalue = e.FormattedValue.ToString();
            // Confirm that the cell is not empty.


            if (string.IsNullOrEmpty(e.FormattedValue.ToString()))
            {
                positionsDataGridView.Rows[e.RowIndex].ErrorText =
                headerText + " cannot be empty";
                e.Cancel = true;
            }
            if (headerText == "PositionId" | headerText == "PositionName")
            {
                foreach (DataGridViewRow row in positionsDataGridView.Rows)
                {
                    foreach (DataGridViewColumn col in positionsDataGridView.Columns)
                    {

                        if (row.Index < positionsDataGridView.Rows.Count - 1)
                        {
                            if (col.Index != positionsDataGridView.CurrentCell.ColumnIndex | row.Index != positionsDataGridView.CurrentCell.RowIndex)
                            {
                                //Regex.IsMatch(LN1, LN2, RegexOptions.IgnoreCase
                                var compare = row.Cells[col.Index].Value.ToString();
                                if (col.HeaderText.ToString() == "PositionId" | col.HeaderText.ToString() == "PositionName")
                                {
                                    if (System.Text.RegularExpressions.Regex.IsMatch(compare, "^" + cellvalue + "$", RegexOptions.IgnoreCase))
                                    {
                                        positionsDataGridView.Rows[e.RowIndex].ErrorText = headerText + " cannot contain duplicate";
                                        e.Cancel = true;
                                    }
                                }



                            }

                        }
                    }
                }
            }
        }
    }
    }
}
