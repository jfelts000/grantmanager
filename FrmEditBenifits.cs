﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrantManager
{
    public partial class FrmEditBenifits : Form
    {
        public FrmEditBenifits()
        {
            InitializeComponent();
        }

        private void benifitsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.benifitsBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.grantManagerDataSet);

        }

        private void FrmEditBenifits_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'grantManagerDataSet.Benifits' table. You can move, or remove it, as needed.
            this.benifitsTableAdapter.Fill(this.grantManagerDataSet.Benifits);

        }

        private void benifitsDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void benifitsDataGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            string headerText = benifitsDataGridView.Columns[e.ColumnIndex].HeaderText;
            string cellvalue = e.FormattedValue.ToString();
            // Confirm that the cell is not empty.


            if (string.IsNullOrEmpty(e.FormattedValue.ToString()))
            {
                benifitsDataGridView.Rows[e.RowIndex].ErrorText =
                headerText + " cannot be empty";
                e.Cancel = true;
            }
            if (headerText == "BenifitId" | headerText == "BenifitnName")
            {
                foreach (DataGridViewRow row in benifitsDataGridView.Rows)
                {
                    foreach (DataGridViewColumn col in benifitsDataGridView.Columns)
                    {

                        if (row.Index < benifitsDataGridView.Rows.Count - 1)
                        {
                            if (col.Index != benifitsDataGridView.CurrentCell.ColumnIndex | row.Index != benifitsDataGridView.CurrentCell.RowIndex)
                            {
                                //Regex.IsMatch(LN1, LN2, RegexOptions.IgnoreCase
                                var compare = row.Cells[col.Index].Value.ToString();
                                if (col.HeaderText.ToString() == "BenifitId" | col.HeaderText.ToString() == "BenifitName")
                                {
                                    if (System.Text.RegularExpressions.Regex.IsMatch(compare, "^" + cellvalue + "$", RegexOptions.IgnoreCase))
                                    {
                                        benifitsDataGridView.Rows[e.RowIndex].ErrorText = headerText + " cannot contain duplicate";
                                        e.Cancel = true;
                                    }
                                }



                            }

                        }
                    }
                }
            }
        }
    }
    }
}
